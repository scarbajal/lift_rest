package ca.polymtl.log4420.itemrest
package rest

// ItemRest
import model.Item

// Lift
import net.liftweb._
import common.Box

// Lift http
import http.rest.RestHelper

// Lift json
import json._

object ItemUpdateWebService extends RestHelper
{
  // Serve /api/item and friends
  serve( "api" / "item" prefix {

      // DELETE the item in question
      case itemId :: Nil JsonDelete _ => Item.deleteById( itemId ): Box[JValue]

    // POST adds the item if the JSON is parsable
    case Nil JsonPost json -> _  => Item.createFromJson( json ) : Box[JValue]

    // PUT if we find the item, merge the fields from the
    // the PUT body and update the item
    case itemId :: Nil JsonPut json -> _ => Item.updateById( itemId, json ) : Box[JValue]
  })
}
