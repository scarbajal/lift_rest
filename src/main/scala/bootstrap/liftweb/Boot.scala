package bootstrap.liftweb


import net.liftweb.http.LiftRules

import ca.polymtl.log4420.itemrest._
import rest._
import lib.RestMongo
import fixture._

class Boot
{
  def boot
  {
    RestMongo.start()
    ItemFixture.insertIfEmpty()

    LiftRules.statelessDispatchTable.append(ItemWebService)
    LiftRules.statelessDispatchTable.append(ItemUpdateWebService)

    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))
  }
}


